class PokeApi { // eslint-disable-line no-unused-vars
  search( id ) {
    // eslint-disable-next-line no-undef
    this.request = fetch( `https://pokeapi.co/api/v2/pokemon/${ id }` );
    return this.request.then( data => data.json() );
  }
}
